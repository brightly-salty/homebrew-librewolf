# Fabrizio Librewolf

## How do I install these formulae?

`brew install fabrizio/homebrew-librewolf https://gitlab.com/fabrizio/homebrew-librewolf` or `brew tap fabrizio/homebrewlibrewolf https://gitlab.com/fabrizio/homebrew-librewolf` and `brew install librewolf`

## Documentation

`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).
