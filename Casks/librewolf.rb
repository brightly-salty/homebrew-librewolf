cask "librewolf" do
  version "89.0-1"
  sha256 "770fb7c8e341a967c77c69baae44ea35c489b4ebaf5059a2f63a377130f9958e"

  url "https://gitlab.com/librewolf-community/browser/macos/uploads/b9599a19f0da293bb37d28d8e56b48bc/librewolf-#{version}.dmg",
      verified: "gitlab.com/librewolf-community/browser/macos"
  name "LibreWolf"
  desc "A fork of Firefox, focused on privacy, security and freedom"
  homepage "https://librewolf-community.gitlab.io/"

  app "LibreWolf.app"

  zap trash: [
    "~/Library/Application Support/LibreWolf",
    "~/Library/Caches/LibreWolf",
    "~/Library/Caches/LibreWolf Community",
    "~/Library/Preferences/io.gitlab.librewolf-community.librewolf.plist",
    "~/Library/Saved Application State/io.gitlab.librewolf-community.librewolf.savedState",
    "~/.librewolf",
   ]
end
